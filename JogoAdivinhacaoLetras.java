package servidor;


package servidor;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.jws.WebService;


@WebService
public class JogoAdivinhacaoLetras {
    
      List<Jogador> jogadores = new ArrayList();
    
    //Esta funcao sera responsavel por mostrar o comando para cada uma das funcoes assim como uma breve explicacao para as mesmas
    public String[] listarComandos(){
        
        String[] comandos = new String[6];
        comandos[0] = "Registrar novo Jogador";
        comandos[1] = "Iniciar o jogo: Esta funcao fara com que o servidor inicie o jogo enviando para os jogadores uma mensagem informativa junto da primeira palavra, comando /ir";
        comandos[2] = "Nova Rodada: Esta funcao ira creditar os pontos para aquele que acertar e enviar uma nova palavra, comando /nova";
        comandos[3] = "Finalizar o jogo: Esta funcao ira finalizar a partida alem de enviar uma mensagem informando o vencedor, comando /fim";
        comandos[4] = "Mostrar Vencedor";
        comandos[5] = "Mostrar a quantidade de Jogadores";
        return comandos;
      }
    
    
    
     public void registrar(Jogador jogador){ // Função 1
        jogadores.add(jogador);
    }
     
     public String[] novoJogo(){  // Função 2
        int i=0;
        ArrayList<String> palavrasPossiveis = new ArrayList<String>();
        palavrasPossiveis.add("Augustinopolis&14");
        palavrasPossiveis.add("Caraguatatuba&13");
        palavrasPossiveis.add("Cordeiropolis&13");
        palavrasPossiveis.add("Esperantinopolis&16");
        palavrasPossiveis.add("Itaquaquecetuba&15");
        palavrasPossiveis.add("Paranapiacaba&13");
        palavrasPossiveis.add("Constitucionalissimamente&25");
        Random rand = new Random();
        String palavraChave;
        String[] palavraSelecionada = new String[2];
        int indexRand =  rand.nextInt(6);
        palavraChave = palavrasPossiveis[indexRand];
        palavraSelecionada = palavraChave.split("&");
        i++;        
        return palavraSelecionada;
    }
       
    //Implementacao parcial da funcao de nova rodada que escolhe uma palavra entre as possiveis
    public String[] novaRodada(){  // Função 3
        int i=0;
        ArrayList<String> palavrasPossiveis = new ArrayList<String>();
        palavrasPossiveis.add("Augustinopolis&14");
        palavrasPossiveis.add("Caraguatatuba&13");
        palavrasPossiveis.add("Cordeiropolis&13");
        palavrasPossiveis.add("Esperantinopolis&16");
        palavrasPossiveis.add("Itaquaquecetuba&15");
        palavrasPossiveis.add("Paranapiacaba&13");
        palavrasPossiveis.add("Constitucionalissimamente&25");
        Random rand = new Random();
        String palavraChave;
        String[] palavraSelecionada = new String[2];
        int indexRand =  rand.nextInt(6);
        palavraChave = palavrasPossiveis[indexRand];
        palavraSelecionada = palavraChave.split("&");
        i++;        
        return palavraSelecionada;
    }
    public void finalizarJogo(){ // Função 4
    	
    }
    
    //Esta funcao ira percorrer o array de jogadores ate encontrar o jogador de maior pontuacao e retorna-lo
    public Jogador mostraVencedor(List<Jogador> jogadoresFinal){ // Função 5
    	Jogador vencedor = new Jogador("semNome", 0);
    	
    	for(Jogador p : jogadoresFinal){
    		p.getPontuacao();
    		if(vencedor.getPontuacao() < p.getPontuacao()){
    			vencedor = p;
    		}  		
    	}
    	
    	return vencedor; 	
    }
    
    
    public int qtdJogadores(){  // Função 6
        return jogadores.size();
    }
    
   
   
}
