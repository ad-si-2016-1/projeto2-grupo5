1) Descrição
-

1.1) Resumo
-

Trata-se de um jogo no qual o servidor seleciona uma palavra (randômica) que possua quantidade significativa de letras(que é mostrada ao jogador) e armazena a resposta contendo o número de letras que a mesma possui. 


1.2) Quantidade de jogadores
-

O jogo pode ser jogado com múltiplos usuários.


1.3) Regras
-

Os jogadores devem dar seu palpite a respeito da palavra (quantas letras a mesma possui), caso o palpite seja correto  o jogador vence a rodada, caso nao acerte uma nova rodada é iniciada com uma nova palavra sendo exibida aos jogadores. Ao final de 10 rodadas de palavras e palpites aquele jogador que tiver somado mais pontos sairá como o vencedor, caso o jogo empate mais duas palavras serao selecionadas para fazer o desempate.


1.4) Como vencer
-

Vence o jogador em que ao final de 10 rodadas de palavras e palpites acumular mais pontos.