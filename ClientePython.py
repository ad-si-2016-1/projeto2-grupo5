import xml
import suds
from suds.client import Client
import xml.etree.ElementTree as ET
import platform
import os
import sys


url = "http://localhost:8080/WebServices/jogoAdivinhacaoLetras?wsdl"

cliente = Client(url)
totaljogadores = ''
caracterjogador = ''

#Função para cadastrar os jogadores.
def CadastraJogador():                             
    cliente = Client(url)
    print('Digite um nome para o jogador: ')
    try:
        nome = str(sys.stdin.readline().encode())
    except SyntaxError, NameError:
        print ("Erro! Digite um nome novamente (tente não utilizar caracteres especiais)")
    else:
        Jogador = cliente.factory.create('jogador')
        Jogador.nomeJogador = nome
        totaljogadores = cliente.service.getListaClientes()
        if len(totaljogadores)==0:
           Jogador.caractereJogo = 'X'
        else:
           Jogador.caractereJogo = 'O'
        global caracterjogador
        caracterjogador = Jogador.caractereJogo
        cliente.service.registrarCliente(Jogador)
        print(" Jogador conectado -->  " + Jogador.nomeJogador)

def Listarjogadores():                                   
    cliente = Client(url)
    b = "()={}"
    totaljogadores = cliente.service.getListaClientes()
    str(totaljogadores).lstrip('[').rstrip(']')          
    for i in range(len(totaljogadores)):
        individuo = str(totaljogadores[i])
        i + 1
        for caractere in b:
            individuo = individuo.replace(caractere,"")
        print(individuo[7:])
   totaljogadores = cliente.service.getListaClientes()
   for item in totaljogadores:
        for data in item.jogador:
            print data.nomeJogador

    nomejogadores = cliente.service.getNomeJogadores()
    str(nomejogadores).lstrip('[').rstrip(']')          
    for i in range(len(nomejogadores)):
        individuo = nomejogadores[i]
        i + 1
        for caractere in b:
            individuo = individuo.replace(caractere,"")
            print(individuo)
   
    print(nomejogadores)

#Função de início de jogo (a ser implementada)
def IniciarJogo():

#Função de nova rodada (a ser implementada)
def NovaRodada():

#Mostra quantidade de jogadores conectados
def qtdJogadores():
	totaljogadores = cliente.service.getListaClientes()
	print(totaljogadores)

#Função 5 - Função não completada
def MostraVencedor():
	#Jogador vencedor = new Jogador("semNome", 0);
    	
    	#for(Jogador p : jogadoresFinal)
    		#p.getPontuacao();
    		#if(vencedor.getPontuacao() < p.getPontuacao()){
    			#vencedor = p;
    		  		
    	
    	
    	#return vencedor; 

class limparcache(suds.plugin.MessagePlugin):
    def __init__(self, *args, **kwargs):
        self.last_payload = None


    def received(self, context):
        #recieved xml as a string
        #print "%s bytes recieved" % len(context.reply)
        self.last_payload = context.reply

        #clean up reply to prevent parsing
        context.reply = ""
        return context

def main():
    RegistraJogador()
    while True:
        print("---- Bem Vindo ao Adivinha quantas Letras Tem ?? ----")
        print(" 1 - Registrar Jogador ")
        print(" 2 - Iniciar Jogo ")
        print(" 3 - Nova Rodada ") 
        print(" 4 - Finalizar Jogo ")                    
        print(" 5 - Mostrar Vencedor ")
        print(" 6 - Mostrar Quantidade de Jogadores ")
        print("-----------------------")
        try:
            opcao = input("Digite a função desejada : ")
        except NameError:
            print("Função invalida, verifique e tente novamente")
        else:
            if opcao == 1:
              RegistraJogador()
            elif opcao == 2:
              InicarJogo()
            elif opcao == 3:
              NovaRodada()
            elif opcao == 4:            #Função 4
              break;
            elif opcao == 5:
              MostraVencedor()
            elif opcao == 6:
			  qtdJogadores()
            else:
                Limpa()

if __name__ == '__main__':
    main()
