package master;
	
	//Yago Pereira Zardo

	import java.util.ArrayList;

	import javax.swing.JOptionPane;

	import servidor.Jogador;
	import servidor.JogoAdivinhacaoLetras;
	import servidor.JogoAdivinhacaoLetrasService;

	public class Cliente {

		public static void main(String[] args) {

			JogoAdivinhacaoLetrasService service = new JogoAdivinhacaoLetrasService();
			JogoAdivinhacaoLetras port = service.getJogoAdivinhacaoLetrasPort();
			
			boolean valid = true;
			while (valid) {

				try {

					ArrayList<String> comandos =  (ArrayList<String>) port.listarComandos();
					for (int i = 0; i < comandos.size(); i++) {
					
						 System.out.println(comandos.get(i));
					}
					
			        ArrayList<Jogador> jogadores = new ArrayList<Jogador>();
					int opcao = Integer.parseInt(JOptionPane.showInputDialog(null,"Digite uma opcao"));
					switch (opcao) {
					
					case 1:
					
						Jogador jogador = new Jogador();
						jogador.setNome("Yago");
						jogador.setPontuacao(0);
						port.registrar(jogador);
						jogadores.add(jogador);
						break;
						
					case 2:
						
						JOptionPane.showMessageDialog(null, "Nova jogo: " + port.novoJogo());
						break;
						
					case 3:
					
						JOptionPane.showMessageDialog(null, "Nova rodada: " + port.novaRodada());
						break;
						
					case 4:
					
						port.finalizarJogo();
						break;
						
					case 5:
					
						Jogador vencedor = port.mostraVencedor(jogadores);
						JOptionPane.showMessageDialog(null, "O vencedor foi: " + vencedor.getNome()+ "Sua pontuacao foi: " + vencedor.getPontuacao() );
						break;
						
					case 6:
					
						JOptionPane.showMessageDialog(null, "" + port.qtdJogadores());
						break;
						
					case 7:
					
						valid = false;
						System.exit(0);
						break;

					default:
						break;
					}
				 }catch (Exception e) {
				
					JOptionPane.showMessageDialog(null, "Opcao invalida");
				 }												
			 }
		 }
	 }

